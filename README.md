# Text Cloak - Privacy Anonymization for Scene Images
(*Please note that this project is actively under development, and updates are in progress to add functionality and features.)

Text Cloak is a Python-based project that leverages computer vision and natural language processing techniques to automatically detect and anonymize private information within scene images. This project is presented as a Streamlit web application, making it user-friendly and accessible.

![Text Cloak App Screenshot](./resources/text_cloak_app_screenshot.png)

## Features

- Automatic detection of private information in scene images, including text and sensitive data.
- Real-time anonymization of detected information, ensuring privacy and security.
- User-friendly web interface powered by Streamlit for easy interaction and customization.
- Highly customizable and extendable to support various image types and data formats.

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/andreianikolau/text-cloak.git
   cd text-cloak

2. Install the required dependencies using pip or conda
`pip install -r requirements.txt´
or 
`conda env create -f environment.yml´

3. run streamlite app
`streamlit run app.py´

4. Access the Text Cloak app in your web browser at http://localhost:8501.

## Usage
1.  Upload an image: Select a scene image that contains private information you want to anonymize.
2. Automatic Detection: The application will automatically detect private information within the image, such as text, credit card numbers, or sensitive data.
3. Anonymization: Identified private information will be obscured or replaced with placeholders to ensure privacy.
4. Download: Save the anonymized image to your local device or share it securely.


Detection Algorithm: We are using MMOCRYou can choose from a range of detection algorithms such as Optical Character Recognition (OCR), object recognition, and more.

Anonymization Strategy: Define how you want to anonymize the detected information, e.g., by blurring, pixelation, or replacing with placeholders.

Supported Image Types: Extend the project to support various image types and formats.* jpge

Contributing
We welcome contributions from the open-source community to improve and expand Text Cloak. Whether it's fixing bugs, adding new features, or enhancing the user interface, your contributions are highly appreciated. Please follow the guidelines in the CONTRIBUTING.md file.

License
Text Cloak is open-source and distributed under the MIT License.

Contact
For questions or issues, contact us.