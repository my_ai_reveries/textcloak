# %%
import streamlit as st
import os
from textCloakManager import TextCloak
from textCloakUtils.utils import pil_image_to_b64, b64_to_pil_image
from PIL import Image
import re
import pandas as pd

version = "0" #TODO setuptools version and auto increase
# if os.environ.get("DOCKER_CUSTOM_IMAGE_NAME") is not None:
#     arr = os.environ.get("DOCKER_CUSTOM_IMAGE_NAME").split(":")
#     if len(arr) == 2:
#         version = arr[1]

st.write("Running version: " + version)


RED = "#F75D59"
YELLOW = "#F5F5D6"


# Load Text Cloak
# ----------
@st.cache(allow_output_mutation=True, show_spinner=False)
def load_text_cloak():
    return TextCloak ()


textCloak = load_text_cloak()

# App
# ---
st.title("Text Cloak")

st.markdown(""" <style> .font {
font-size:0.9rem;} 
</style> """, unsafe_allow_html=True)


def color_cell(subset_df):
    yellow = f'background-color: {YELLOW}'
    red = f'background-color: {RED}'
    default = ''
    colors_retrieved = [default, default, default]
    if subset_df['Status'] == "Text Detection Failed":
        colors_retrieved[0] = red
        return colors_retrieved
    if not subset_df['Anonymized?'] and subset_df['Needs Manual?']:
        colors_retrieved[1] = colors_retrieved[2] = yellow
    return colors_retrieved


def color_expander(nthChild, color):
    return f""" <style> div[data-testid="stExpander"]:nth-child( {nthChild} ) ul li div[tabindex="0"]{{
        background-color: {color};
    }}</style> """


def convert_df(df):
    return df.to_csv(index=False).encode('utf-8')



def create_table_row(idx, result):
    if result.isAnonymized or result.isAnonymizedforText:
        res = re.search(
            r"\n.*(#VIN: (?P<vins>[0-9]+))\n.*(#Faces: (?P<faces>[0-9]+))\n.*(#LPlates: (?P<plates>[0-9]+))\n.*(#TextInfo: (?P<textinfo>[0-9]+))\n.*(Total Detections: (?P<total>[0-9]+))",
            result.message)
        return [image_file_buffers[idx].name, result.imageType, result.status, result.confidenceLevel,
                result.isAnonymized, result.manualAnonymization, result.isAnonymizedforText, result.manualAnonymizationforText, 
                int(res.group('vins')), int(res.group('plates')),
                int(res.group('faces')), int(res.group('textinfo')), int(res.group('total'))]
    return [image_file_buffers[idx].name, result.imageType, result.status, result.confidenceLevel, result.isAnonymized,
            result.manualAnonymization, result.isAnonymizedforText, result.manualAnonymizationforText, 0, 0, 0, 0, 0]


# @st.cache(allow_output_mutation=True, show_spinner=True)
def detect_and_mask_text(b64):
    return textCloak.run_text_cloak(b64)


headers = ["Image", "Status", "Anonymized?", "Needs Manual?",  "#Total Secret Text Detections"]
rows = []
b64s = []
submit = False
pil_images = []
mask_pil_images = []
results = []

image_file_buffers = st.file_uploader("Upload image", type=["jpg", "jpeg"], accept_multiple_files=True)

if image_file_buffers is not []:

    pil_images = list(map(lambda x: Image.open(x), image_file_buffers))
    b64s = list(map(lambda x: pil_image_to_b64(x), pil_images))
    if len(image_file_buffers) == len(b64s):
        submit = st.button('anonymize')

if submit:

    with st.spinner('Wait for it...'):
        results = list(map(lambda x: detect_and_mask_text(x), b64s))

    anon_pil_images = list(map(lambda x: b64_to_pil_image(x.outputImage), results))
    rows = [create_table_row(i, x) for i, x in enumerate(results)]

    df = pd.DataFrame(rows, columns=headers)
    df_highlight = df.style.apply(color_cell, subset=['Status', 'Anonymized?', 'Needs Manual?' ], axis=1)
    st.table(df_highlight)

    st.write("**WARNING**: by clicking download, the page will refresh and you will lose the preview data")
    st.download_button(
        "Download CSV",
        convert_df(df),
        "text-cloak-summary.csv",
        "text/csv",
        key='download-csv'
    )

    container = st.container()
    for idx in range(0, len(pil_images)):

        if results[idx].status == "Text Detection Failed":
            st.markdown(color_expander(idx + 1, RED), unsafe_allow_html=True)
        elif (not results[idx].isAnonymized and results[idx].manualAnonymization):
            st.markdown(color_expander(idx + 1, YELLOW), unsafe_allow_html=True)
        

        exp = container.expander(image_file_buffers[idx].name)
        exp.write("")
        cols = exp.columns(2)
        cols[0].image(pil_images[idx], caption='original image')
        cols[1].image(mask_pil_images[idx], caption='processed image')
        exp.table(df.loc[[idx]])

# %%
