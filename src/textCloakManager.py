from textCloakUtils.textReader import run_mmocr
from textCloakUtils.textBlendStream import run_text_blend
from textCloakUtils.textFilter import run_text_filter
from textCloakUtils.maskText import mask_secret_text
from textCloakUtils.utils import pil_image_to_b64, b64_to_pil_image
import logging


logger = logging.getLogger(__name__)


class TextCloak():
    """
    This class implements an anonymization pipeline for secret text (email, phone, url) that aggregates all steps needed.
    """
    
    def __init__(self):
        # Logging
        
        logger.info("Text Cloak Initialization")
        
    def _initialize_output(self):
        # Initialize output var to default values
        self.outputImage: str = ""
        self.manualAnonymization: bool = False
        self.isAnonymized: bool = False
        self.status: str = ""
        self.message: str = ""
    
    def _get_output(self):
              return  vars(self)
        
        
    def _buildMessage(self, text):
        #TODO create exception messages
        self.message = f"""TEXT CLOAK RESULTS
                        Total Text Detections: {len(text)}"""
                        
    def _setStatusSuccessfull(self, success: bool):
        if success:
            self.status = "Text Detection Successfull"
        else:
            self.status = "Text Masking Failed"
        

        
    def run_text_cloak(b64_img):
        self._initialize_output()
        
        logger.info("Text Cloak Processing")
        
        try:
            img = b64_to_pil_image(b64_img)
        
            result = run_mmocr(img) # run ocr to detect text

            merged_bboxes, merged_text = run_text_blend(result) # merge close text to get better results 

            secret_text = run_text_filter(merged_text) # filter emails, phones, url

            img = mask_secret_text(img, secret_text, merged_bboxes) # anonymize filtered 
        
        except Exception as e:
            self.manualAnonymization = True
            #TODO .buildExceptionMessage(str(e))
            self._setStatusSuccessfull(False)
            logger.exception("Text Masking Failed with error: %s", str(e))

        finally:
            self.outputImage = pil_image_to_b64(img)
            logger.info("Text Cloak ==== Text Masking End ====")

        return self._get_output

