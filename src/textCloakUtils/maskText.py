from PIL import Image, ImageDraw, ImageFilter
import random
import numpy as np

def _blur_image(img, bboxes, tags):
    mask_blur = Image.new("L", img.size, 255)
    [ # pylint: disable=W0106
        [ImageDraw.Draw(mask_blur).polygon(box, fill=0) for box in bboxes[i]]
        for i in range(len(bboxes))
        if tags[i]
    ] 
    blurred_image = img.copy()
    blurred_image = blurred_image.filter(ImageFilter.GaussianBlur(10))
    blurred_image.paste(img, mask=mask_blur)

    return blurred_image

def _randomize_image(image, bboxes, tags):
    image_array = np.asarray(image)
    
    # Create a copy of the original image
    for i, box in enumerate(bboxes):
        if tags[i]:
            for coords in box:
                mask_img = Image.new("L", image.size, 0)
                ImageDraw.Draw(mask_img).polygon(coords, fill=1)
                mask = np.array(mask_img)
                to_shuffle = image_array[mask > 0]
                random.shuffle(to_shuffle)
                randomized_array = image_array.copy()

                randomized_array[mask > 0] = to_shuffle
                image_array = randomized_array

    return Image.fromarray(image_array)

def _draw_bbox_image(img, bboxes, tags):
    [ # pylint: disable=W0106
        [
            ImageDraw.Draw(img).polygon(
                box,
                outline=(
                    random.randint(0, 255),
                    random.randint(0, 255),
                    random.randint(0, 255),
                ),
            )
            for box in bboxes[i]
        ] 
        for i in range(len(bboxes))
        if tags[i]
    ]
    return img


def mask_secret_text(img, tags, bboxes, technique='random'):
    technique_anonymization = {'random':_randomize_image,
                                                'blur': _blur_image,
                                                'bbox': _draw_bbox_image
                               }
    img_anonymized = technique_anonymization.get(technique)(img, bboxes, tags)
                   
    return img_anonymized