from shapely.ops import unary_union
from shapely.geometry import Polygon

def _sort_bboxes(data):
    # Combine polygons with their respective rec_texts
    polygons_with_texts = list(zip(data["det_polygons"].copy(), data["rec_texts"].copy()))

    # Sort the polygons based on higher y to lower y and then from left to right
    sorted_polygons_with_texts = sorted(
        polygons_with_texts, key=lambda x: (x[0][1], x[0][0])
    )

    # Separate the sorted polygons and rec_texts
    det_polygons, rec_texts = zip(*sorted_polygons_with_texts)

    return list(det_polygons), list(rec_texts)

# TODO Check this code and change to improve
def merge_adjacent_polygons(polygon1, polygon2, dist=0.8, y_range=0.8):
    x1_min, _, x1_max, y1_max = polygon1.bounds
    x2_min, _, x2_max, y2_max = polygon2.bounds
    height1 = polygon1.bounds[3] - polygon1.bounds[1]
    height2 = polygon2.bounds[3] - polygon2.bounds[1]
    height = min(height1, height2)  # Use min() to select the smaller height
    if (
        x1_max <= x2_min
        and abs(y1_max - y2_max) <= y_range * height
        and x2_min - x1_max <= dist * height
    ):
        return True

    if (
        x2_max <= x1_min
        and abs(y1_max - y2_max) <= y_range * height
        and x1_min - x2_max <= dist * height
    ):
        return True

    return False


def merge_aux(
    merged_polygon, current_polygon, merged_rec_texts, current_rec_text, merged_polygons
):
    merged_polygon = unary_union([merged_polygon, current_polygon])
    merged_rec_text = merged_rec_texts + " " + current_rec_text
    merged_polygons = merged_polygon
    merged_rec_texts = merged_rec_text
    merged = True
    return merged_polygons, merged_rec_texts, merged
def _merge_bboxes(det_polygons, rec_texts):
    merged_polygons = []
    merged_rec_texts = []
    for i, polygon in enumerate(det_polygons):
        # Group the coordinates into pairs
        coordinates = [(polygon[j], polygon[j + 1]) for j in range(0, len(polygon), 2)]
        current_polygon = Polygon(coordinates)
        current_rec_text = rec_texts[i]

        merged = False
        for j, merged_polygon in enumerate(merged_polygons):
            # Check if polygons are adjacent from left to right
            if current_polygon.bounds[2] <= merged_polygon.bounds[0]:
                # Check if polygons are touching/overlapping or almost touching
                if current_polygon.intersects(
                    merged_polygon
                ) or current_polygon.touches(merged_polygon):
                    merged_polygons[j], merged_rec_texts[j], merged = merge_aux(
                        merged_polygon,
                        current_polygon,
                        merged_rec_texts[j],
                        current_rec_text,
                        merged_polygons[j],
                    )
                    break

                # Check if polygons are close enough
                if merge_adjacent_polygons(
                    current_polygon, merged_polygon, dist=1, y_range=0.7
                ):
                    merged_polygons[j], merged_rec_texts[j], merged = merge_aux(
                        merged_polygon,
                        current_polygon,
                        merged_rec_texts[j],
                        current_rec_text,
                        merged_polygons[j],
                    )
                    break
            else:
                # If not, from right to left:
                # Check if polygons are touching/overlapping or almost touching
                if current_polygon.intersects(
                    merged_polygon
                ) or current_polygon.touches(merged_polygon):
                    merged_polygons[j], merged_rec_texts[j], merged = merge_aux(
                        merged_polygon,
                        current_polygon,
                        current_rec_text,
                        merged_rec_texts[j],
                        merged_polygons[j],
                    )
                    break

                # Check if polygons are close enough
                if merge_adjacent_polygons(
                    current_polygon, merged_polygon, dist=1, y_range=0.7
                ):
                    merged_polygons[j], merged_rec_texts[j], merged = merge_aux(
                        merged_polygon,
                        current_polygon,
                        current_rec_text,
                        merged_rec_texts[j],
                        merged_polygons[j],
                    )
                    break
        if not merged:
            merged_polygons.append(current_polygon)
            merged_rec_texts.append(current_rec_text)

    return merged_polygons, merged_rec_texts



def transform_bboxes(bboxes):
    #TODO .. maybe transform in merge and avoid other loop
    transformed_bboxes = []
    for bbox in bboxes:
        if bbox.geom_type == 'Polygon':
         
            x, y = bboxes.exterior.xy
            transformed_bboxes.append([list(zip(x,y))])                 

        elif bboxes.geom_type == 'MultiPolygon':

            sub_polygons = []
            for sub_polygon in bboxes.geoms:
                x, y = sub_polygon.exterior.xy
                sub_polygons.append(list(zip(x,y)))
            transformed_bboxes.append(sub_polygons)
            
            
def run_text_blend(data):
        
    sorted_bboxes, sorted_texts = _sort_bboxes(data)  # Preprocess data
    merged_bboxes, merged_texts = _merge_bboxes(sorted_bboxes, sorted_texts)
    merged_bboxes = transform_bboxes(merged_bboxes)
    return merged_bboxes, merged_texts