from mmocr.apis import MMOCRInferencer
import numpy as np
import os

def model_folder(model: str): 
    absolute_path = os.path.dirname(__file__)
    models = os.path.join(absolute_path, "models")
    return os.path.join(models, model)


def run_mmocr(img, det_model='dbnet', rec_model= 'svtr-small'):

    models = {'dbnet': model_folder('dbnet_resnet50-oclip_1200e_icdar2015_20221102_115917-bde8c87a.pth'),
          'svtr-small': model_folder('svtr-small_20e_st_mj-35d800d6.pth')}

    # Initialize the MMOCR inferencer
    ocr = MMOCRInferencer(rec=rec_model, rec_weights=models[rec_model],
                        det=det_model, det_weights=models[det_model])

    result = ocr(np.asarray(img), save_vis=False, return_vis=False, save_pred=False)['predictions']
    return result

def run_easyocr(img):
    pass