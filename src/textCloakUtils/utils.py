from io import BytesIO
import base64
from PIL import Image


def pil_image_to_b64(pil_image):
    buffered = BytesIO()
    pil_image.save(buffered, format="JPEG")
    b64_string = base64.b64encode(buffered.getvalue())
    return b64_string.decode('utf-8')


def b64_to_pil_image(b64):
    return Image.open(BytesIO(base64.b64decode(b64)))
